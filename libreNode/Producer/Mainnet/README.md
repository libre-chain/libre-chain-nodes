# LIBRE CHAIN MAINNET INFO

You will need to begin on the testnet before running on Libre Mainnet. Once you produce blocks for 2 weeks on Testnet, you will be eligible for Mainnet registration.

The genesis.json and peers.ini are here in this directory. You will need to modify your config.ini and run nodeos with these peers to get the blocks.

## Libre Mainnet Links

**Libre Mainnet Telegram**

https://t.me/+-mn0MPbvvyAxMDcx

**Libre Mainnet Hyperion History API Swagger Docs**

https://lb.libre.org/v2/docs/static/index.html - blocktime / cryptobloks / quantum

**Libre Mainnet Block Explorer**

https://libre-explorer.edenia.cloud/ - edenia

https://lb.libre.org/v2/explore/ - blocktime / cryptobloks / quantum

**Libre Mainnet Network Monitor**

https://libre.antelope.tools - edenia

**Libre Mainnet Snapshots**

https://archive.pdx.cryptobloks.io/snapshots/libre/ - cryptobloks

http://snapshots.eosusa.news/snapshots/libre/ - eosusa

https://libre.snapshots.eossweden.org/ - sweden

**Libre Mainnet Blockchain Monitors**

Libre Mainnet Blockchain Alerts: https://t.me/+6rqJYSdWGs1mZmVh - cryptobloks

Libre Mainnet Blockchain Status: https://t.me/+VfC5D2pZ3X5lZjIx - cryptobloks

Sweden Simple Monitor: https://t.me/c/1519198886/1062 - sweden