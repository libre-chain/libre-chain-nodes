#!/bin/bash
################################################################################
# Libre tools
# Originated from scripts by CryptoLions.io
###############################################################################

source /opt/libre-chain-nodes/libreNode/node.env

if [ -f $DATADIR/nodeos.pid ]; then
    pid=$(cat $DATADIR/nodeos.pid)
    echo $pid
    kill $pid
    
    echo -ne "Stopping Nodeos"
    while true; do
        [ ! -d "/proc/$pid/fd" ] && break
        echo -ne "."
        sleep 1
    done
    rm -r $DATADIR/nodeos.pid
    
    DATE=$(date -d "now" +'%Y_%m_%d-%H_%M')
    if [ ! -d $DATADIR/logs ]; then
        mkdir $DATADIR/logs
    fi

    # Check if stderr.txt and stdout.txt exist before archiving
    if [ -f $DATADIR/stderr.txt ] && [ -f $DATADIR/stdout.txt ]; then
        tar -pcvzf $DATADIR/logs/stderr-$DATE.txt.tar.gz -C $DATADIR stderr.txt stdout.txt
    else
        echo "stderr.txt or stdout.txt not found, skipping archive."
    fi

    echo -ne "\rNodeos Stopped.    \n"
fi