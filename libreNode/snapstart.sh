#!/bin/bash
source /opt/libre-chain-nodes/libreNode/node.env

$MAINDIR/stop.sh
echo -e "Starting Nodeos \n";

ulimit -n 65535
ulimit -s 64000

wget $SNAPSHOTLINK -O $SNAPDIR/latest.bin.tar.gz
cd $SNAPDIR
rm *.bin
tar -zxvf latest.bin.tar.gz
cd $MAINDIR
$NODEOSBINDIR/nodeos --data-dir $DATADIR --config-dir $MAINDIR --snapshot `ls -Art $SNAPDIR/*.bin | tail -n 1` --delete-all-blocks > $DATADIR/stdout.txt 2> $DATADIR/stderr.txt &  echo $! > $DATADIR/nodeos.pid
echo "Started nodeos proc `cat $DATADIR/nodeos.pid` log $DATADIR/stderr.txt"