#!/bin/bash

# Source environment variables
source /opt/libre-chain-nodes/libreNode/node.env

# Start the node with snapshot
cd $MAINDIR
./snapstart.sh

# Wait for node to sync
echo "Waiting for node to sync..."
sleep 60

# Create wallet
echo "Creating wallet..."
./cleos.sh wallet create --name libre --file ~/libre_wallet_password.txt

echo "Setup complete. Your node is now running on the Libre testnet."