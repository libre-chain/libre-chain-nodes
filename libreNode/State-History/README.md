# State History

Running a State History node is an important aspect of the Libre ecosystem. These nodes can provide chain-state history to apps and wallets - one of the top needed services on any chain. You can also build your own applications that may need state history.

If you are building your own app, then you may want to try to run your own Hyperion node as an API so you can have reliable and custom access to the chain for pushing transactions and getting history.
### Hyperion API Node
* Running a Hyperion Node is a massive help to the Libre ecosystem as all apps and wallets will need API access. 
* Hyperion is a node-based API and chain-history service based on Elasticsearch / Kibana [Hyperion API services](https://github.com/eosrio/hyperion-history-api/tree/v3.3.5) .
  * Hyperion stable version is [v3.3.5](https://github.com/eosrio/hyperion-history-api/tree/v3.3.5) // Nodeos [v3.1.0](https://github.com/AntelopeIO/leap/releases/tag/v3.1.0)
  * 2TB NVME is recommended if you intend to run a Hyperion node - we also recommend running this on separate hardware than a producer or seed node

# How to Setup Hyperion History API

Hyperion has 2 main pieces - an API for requests and an indexer that reads state data from the chain SHIP (state history IP). This can be a bit confusing, but everything you need will be in your nodeos config.ini file or provided by the install scripts of Hyperion.

## Operating System and Hardware Requirements
- Bare metal or a physical server with no virtulization in use (Dedicated hardware)
- Ubuntu 20.04 LTS 
- Fast single-threaded CPU 3.5 Ghz+ (the faster, the better)
- 16GB RAM
- 1TB NVME 
- Ubuntu 20.04 (Recommended) 
- Open TCP Ports (80, 443) on your firewall/router  
- Recommend running nginx reverse proxy in front of your hyperion node for rate limiting and whitelist

## Alternative Setup for Nodeos Version Compatible with Hyperion

Download [latest release of of Leap](https://github.com/AntelopeIO/leap/releases/) - we have tested 3.1.0 and it's working with Hyperion 3.3.5 (requires minimum of Ubuntu 18.04) using this experimental install script
```
    cd /opt
    git clone https://gitlab.com/libre-chain/libre-chain-nodes
    cd /opt/libre-chain-nodes/libreNode
    ./Scripts/nodesetup.sh
```

Next clone the v3.3.5 branch of Hyperion

```
git clone --branch v3.3.5 https://github.com/eosrio/hyperion-history-api.git
```

Finally, install Hyperion using scripts located here:
```
cd hyperion-history-api
sudo ./install.sh
sudo ./install_env.sh
```

Create your connections.json from the template by creating a copy:

```
cp example-connections.json connections.json
```

You can use hyp-config to generate the configuration files:
```
./hyp-config new chain libre
```

Check elastic_pass.txt to see if the correct passwords have been set for ampq and elastic in the connections.json
```
cat elastic_pass.txt
nano connections.json
```

We have found that you may need to re-run some of the steps in `install_env.sh` such as the ones related to rabbitmq (ampq):

```
  RABBIT_USER="hyperion" 
  RABBIT_PASSWORD="123456" # set your own password here and update it in `connections.json` 
  sudo rabbitmqctl add_vhost /hyperion
  sudo rabbitmqctl add_user ${RABBIT_USER} ${RABBIT_PASSWORD}
  sudo rabbitmqctl set_user_tags ${RABBIT_USER} administrator
  sudo rabbitmqctl set_permissions -p /hyperion ${RABBIT_USER} ".*" ".*" ".*"
```

Check the chains/libre.config.json and configure the following in the API section:
* server_addr - this will be your hyperion indexer URL (usually 127.0.0.1 unless you are advanced enough to run the indexer and api separately)
* server_port - this will be your indexer port for your API to talk to, take whatever you have in connections.json for this chain and subtract 1. For example if you `cat connections.json` and see `"WS_ROUTER_PORT": 7001` then set `"server_port": 7000` (those should already be the default values)
* provider_name = name you want to show publicly on Hyperion
* provider_url = adds a link from Hyperion web API to your website
* chain_logo_url = set the logo for Libre, we have hosted one for you "https://i.imgur.com/WKBTNkv.png"

```
"server_addr": "127.0.0.1",
"server_port": 7000,
"server_name" = "hyperion.yourdomain.com"
"provider_name" = "yourValidator"
"provider_url" = "https://yourdomain.com"
"chain_logo_url": "https://i.imgur.com/WKBTNkv.png",
```

Example of what your API section should look like if you are Quantumblok:
```
  "api": {
    "enabled": true,
    "pm2_scaling": 1,
    "node_max_old_space_size": 1024,
    "chain_name": "Libre",
    "server_addr": "127.0.0.1",
    "server_port": 7000,
    "server_name": "hyperion.libre.quantumblok.com",
    "provider_name": "Quantumblok.com",
    "provider_url": "https://Quantumblok.com",
    "chain_api": "",
    "push_api": "",
    "chain_logo_url": "https://i.imgur.com/WKBTNkv.png",
    "enable_caching": true,
    "cache_life": 1,
    "limits": {
      "get_actions": 1000,
      "get_voters": 100,
      "get_links": 1000,
      "get_deltas": 1000,
      "get_trx_actions": 200
```

Once you have configured connections.json and chains/libre.config.json, you can start the indexer in ABI scan mode by editing the indexer in chains/libre.config.json:

```
file="chains/libre.config.json"
sed -i'.bak' -e 's/"live_reader": true/"live_reader": false/' $file
sed -i'.bak' -e 's/"abi_scan_mode": false/"abi_scan_mode": true/' $file
```
After these commands, your `chains/libre.config.json` should look like this and your indexer will start in "ABI SCAN MODE":

```
  "indexer": {
    "enabled": true,
    "node_max_old_space_size": 4096,
    "start_on": 0,
    "stop_on": 0,
    "rewrite": false,
    "purge_queues": false,
    "live_reader": false,
    "live_only_mode": false,
    "abi_scan_mode": true,
    "fetch_block": true,
    "fetch_traces": true,
    "disable_reading": false,
    "disable_indexing": false,
    "process_deltas": true,
    "disable_delta_rm": true
  },
```

Now run the indexer:

```
./run.sh libre-indexer
```

Let it run until it says "no blocks are being processed" - then stop it using this command:

```
./stop.sh libre-indexer
```

Then you can start the indexer to read blocks and enable rewrite LIVE MODE:

```
file="chains/libre.config.json"
sed -i'.bak' -e 's/"live_reader": false/"live_reader": true/' $file
sed -i'.bak' -e 's/"abi_scan_mode": true/"abi_scan_mode": false/' $file
sed -i'.bak' -e 's/"rewrite": false/"rewrite": true/' $file
```

```
  "indexer": {
    "enabled": true,
    "node_max_old_space_size": 4096,
    "start_on": 0,
    "stop_on": 0,
    "rewrite": true,
    "purge_queues": false,
    "live_reader": true,
    "live_only_mode": false,
    "abi_scan_mode": false,
    "fetch_block": true,
    "fetch_traces": true,
    "disable_reading": false,
    "disable_indexing": false,
    "process_deltas": true,
    "disable_delta_rm": true
  },
```

If the indexer is having trouble, then you can disable "live_reader" change the "start_on" and "stop_on" blocks and do 1000000 at a time until you catch up with the chain, then turn the live reader on.

Once that's running you can start the API:

```
./run.sh libre-api
```

Point your DNS to "hyperion.yourdomain.com" and then check your API by making some queries on cleos or using the Swagger at http://hyperion.yourdomain.com/v2/docs