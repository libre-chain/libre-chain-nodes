# Guide to Installing Libre Testnet Nodes

## Super quickstart guide:

1. Install Libre node software:
   ```
   bash -c "$(curl -sSfL https://gitlab.com/libre-chain/libre-chain-nodes/-/raw/main/install.sh)"
   ```

2. Update your environment:
   ```
   source ~/.bashrc
   ```

3. Check your logs:
   ```
   tail -f /opt/libre-chain-nodes/libreNode/stderr.txt
   ```

That's all! Your Libre node should now be running on Testnet. 

To run on Mainnet, you will need to grab peers.ini from the mainnet directory and update the config.ini file, then switch the snapshot link in the node.env file to the mainnet snapshot.

For more advanced configuration and usage, please refer to the full documentation.

----------------------------------------------------------------------------------

There are a few types of nodes you can run on Libre to help the network. Recommendations for running a node include having datacenter-quality redundant internet / power backups, technical knowledge of Linux, and a good attitude!

* Seed Nodes - allow p2p traffic for synchronizing block data
* API Nodes - allow people to submit transactions
* Hyperion Nodes - give access to historical data for APIs
* Validator Nodes - also known as Producers - produce and sign blocks with a signature and get paid

Nodes all run the software called `nodeos` and there are various roles that your node can provide, based on the configuration you set in your `config,ini`. 

There are a few versions of the config.ini in this repo. You can use these examples to modify and run your local config.ini. You might want to take a minute to familiarize yourself with the differences before continuing.

* Basic config.ini [libreNode/config.ini](libreNode/config.ini) - minimum you will need to get started synchronizing blocks. This node can be used as a "seed" node on the network to provide block sync to other nodes. 
* Validator config.ini - [libreNode/Producer/config.ini](libreNode/Producer/config.ini) - minimum you will need to actualy sign and validate blocks.
* State History config.ini [libreNode/State-History/config.ini](libreNode/State-History/config.ini) - specifically tweaked for providing state history - which can be used by [Hyperion History API](https://github.com/eosrio/hyperion-history-api/tree/v3.3.5)
* Advanced config.ini [libreNode/Advanced/config.ini](libreNode/Advanced/config.ini) - can be used to tweak various settings.

## Important! 
* We highly recommend not running your node as the root user! Look up `useradd` and `visudo` if you need guidance on adding a new user account to your server.
* [Troubleshooting](#troubleshooting) for troubleshooting your node.
* [Wallet README](Wallet/) about setting up your wallet.
* [Validator README](libreNode/) about hardware requirements for running a validator / producer node.
* [State History README](libreNode/State-History/) about running state history (thank you!).
* If you install to a nonstandard location, you will need to find and replace the path "/opt/libre-chain-nodes/libreNode" with your new path using something like this: `grep -rl "/opt/libre-chain-nodes/libreNode" | xargs sed -i 's| /opt/libre-chain-nodes/libreNode|/newpath|g'`
----------------------------------------------------------------------------------

## Chain Info

* Genesis.json - Please see the most recent [genesis.json](libreNode/genesis.json) file.
* P2P endpoints - All updated in the [peers.ini](libreNode/peers.ini) file.

----------------------------------------------------------------------------------

## Links

**Libre Testnet Telegram**

https://t.me/+BWHs_Biz7UdiY2Ix

### Links to tools provided by validators

**Libre Local Development Testnet**

https://github.com/edenia/libre-local - edenia

**Libre Testnet History API (Swagger Docs)**

https://testnet.libre.org/v2/docs/static/index.html - blocktime / cryptobloks / quantum

**Libre Testnet Block Explorer**

https://testnet.libre.org/v2/explore/ - blocktime / cryptobloks / quantum

**Libre Testnet Network Monitor**

https://libre-testnet.antelope.tools - edenia

**Libre Testnet Snapshots**

https://archive.pdx.cryptobloks.io/snapshots/libre/ - cryptobloks

http://snapshots.eosusa.news/snapshots/libretestnet/ - eosusa

https://libre-test.snapshots.eossweden.org/ - sweden

**Libre Testnet Blockchain Monitors**

Libre Testnet Blockchain Alerts: https://t.me/+IRu0UoMcGwlhZmMx - cryptobloks

Libre Testnet Blockchain Status: https://t.me/+OhLihPrdqH4xN2Ux - cryptobloks

Sweden Simple Monitor: https://t.me/c/1519198886/1062 - sweden

----------------------------------------------------------------------------------

# To run a Libre Testnet node you need install Leap software. You can compile from sources or install from precompiled binaries.

# We highly recommend using Ubuntu 22.04 LTS 
If you are using Ubuntu 18.04 LTS or another distribution - then you can install LEAP v3.1.0 and build from scratch or use the existing releases in this [Antelope Leap repo](https://github.com/AntelopeIO/leap).

## 1. Manual Installation  

You can either install from source or precompiled binaries. Precompiled is faster, but source is preferrable to advanced users.
### 1.1 Installing from sources  

A. Create folder, download sources, compile and install:  

```
sudo chown `whoami` /opt
mkdir /opt/libre/src  
cd /opt/libre/src  

git clone -b v3.1.0 https://github.com/AntelopeIO/leap.git    
cd leap 

git submodule update --init --recursive   

./scripts/install_deps.sh
./scripts/pinned_build.sh
```  

B. Copy binaries to keep old versions and make sym link to latest:  

```
mkdir /opt/libre
mkdir /opt/libre/v5.0.2
cp /opt/libre/src/leap/build/programs/nodeos/nodeos /opt/libre/v5.0.2/
cp /opt/eosio/src/leap/build/programs/cleos/cleos /opt/libre/v5.0.2/
cp /opt/eosio/src/leap/build/programs/keosd/keosd /opt/libre/v5.0.2/
ln -sf /opt/libre/v5.0.2 /opt/libre/bin
```

Now /opt/libre/bin will point to latest binaries.  

### 1.2 Semi-Auto Install [Precompiled binaries]

A.1 Download the [latest release of Leap](https://github.com/AntelopeIO/leap/releases/)
```
sudo chown `whoami` /opt
mkdir -p /opt/libre/src
cd /opt/libre/src/  
wget https://github.com/AntelopeIO/leap/releases/download/v5.0.2/leap_5.0.2_amd64.deb   
```

To install it - use apt:  
```
sudo apt install ./leap_5.0.2_amd64.deb   
```
It will download all dependencies and install cleos, nodeos, and keosd to /usr/bin  

Next setup your libre scripts and configs.
## 2. Install Libre Testnet Node Configs and Scripts 

```
    cd /opt
    git clone https://gitlab.com/libre-chain/libre-chain-nodes
    cd /opt/libre-chain-nodes/libreNode
```
Now you can try to run a node

**In case you use a different data-dir folders -> edit all paths in files cleos.sh, start.sh, stop.sh, config.ini, Wallet/start_wallet.sh, Wallet/stop_wallet.sh**

### 2.1 Create Validator Account
-  to create an account on Libre test first you will need to create a key
   -  to create a key use `./generate_key.sh keyname` --> this will create a text file named "keyname" with the keys you generated you can also run  `./cleos.sh create key --to-console` if you do not want to create files. Please note - the public key you created should begin with "EOS" and the private key should begin with "5" - to not share the private key with anyone or they will be able to control your account.
   -  Once you have a key, to create an account on Testnet, go to https://libre-testnet.antelope.tools/faucet and use your public key to create an acccount 
   -  If you have issues with the faucet or any other questions, please join the [Libre Testnet Telegram](https://t.me/+BWHs_Biz7UdiY2Ix)

### 2.2 Edit config.ini:  
Update your config.ini so your nodeos knows what do do! 
  - Add the updated peers list by copying and pasting the list from [peers.ini](libreNode/peers.ini) into the bottom of your config.ini file or run `cat peers.ini >> config.ini` 

### 2.3 Run nodeos 
Run the nodeos software and start getting blocks!!
  - To run use `./start.sh --genesis-json ./genesis.json --delete-all-blocks` to start nodeos fresh with all blocks deleted
  
### 2.4 Verify
Final step is to make sure you are actually getting blocks! 
   - To check logs after running nodeos use `tail -f stderr.txt` - if you see ranges synchronizing, then you are good... if you see other things, then check your config.ini and ask around in Telegram for help
  - If there are issues, run `./stop.sh` and even `killall nodeos` then start again using the command above - see [troubleshooting](#troubleshooting) section below! 

----------------------------------------------------------------------------------
## What's next??
So you want to be a validator/producer? Check out the [Producer README](libreNode/README.md)!!! 
- Once you get the chain running with the basic config.ini - you can setup a producer using the config.ini in the [Producer](libreNode/Producer/) directory. 

=================================================================================== 
# Troubleshooting

## Common Issues
* Installing libre-chain-nodes to a diff path?
  * If you have installed to a different directory, you will need to find and replace this line to point to the correct node.env for the scripts to work: `source /opt/libre-chain-nodes/libreNode/node.env`
  * This is a sed command that will recusively change to your correct path if you run it from the 
  * `find . -type f -name "*.sh" -exec sed -i "s|/opt/libre-chain-nodes/libreNode/node.env|$PWD/libreNode/node.env|g" {} ";"`

* Incorrect peers or wrong genesis.json
  * Very important to make sure you have the correct peers in your config.ini and genesis.json specified when you run nodeos (via [start.sh](libreNode/start.sh))

* Incorrect nodeos command
  - Please note - The first run of nodeos should be with --delete-all-blocks and --genesis-json `./start.sh --delete-all-blocks --genesis-json genesis.json`
  - If there are issues, run `./stop.sh` and even `killall nodeos` then start again using the command above! 

* start.sh misconfigured
  * Is your start.sh misconfigured? Take a look at the paths and make sure that the bin directory is correctly specified to the location of your nodoes. Try typing `which nodeos`

* If you have issues with the faucet or any other questions, please join the [Libre Testnet Telegram](https://t.me/+BWHs_Biz7UdiY2Ix)

## How to check logs:
  - To check logs after running nodeos use `tail -f stderr.txt` - if you see ranges synchronizing, then you are good... if you see other things, then check your config.ini and ask around in Telegram for help
  - Check if node is running ok then you should see ranges of blocks synchronizing, if not, then run `./stop.sh` or `killall nodeos` - change your config.ini settings and try again.


