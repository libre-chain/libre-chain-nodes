#!/bin/bash

# Set up environment variables
INSTALL_DIR="/opt/libre-chain-nodes"
REPO_URL="https://gitlab.com/libre-chain/libre-chain-nodes.git"
LEAP_VERSION="5.0.2"
TEMP_DIR="/tmp/libre_install"

# Function to check if a port is in use and find the next available port
find_available_port() {
    local port=$1
    while netstat -tuln | grep -q ":$port "; do
        port=$((port+1))
    done
    echo $port
}

# Function to check if directory is empty
is_directory_empty() {
    [ -z "$(ls -A "$1")" ]
}

# Check if INSTALL_DIR exists and is not empty
if [ -d "$INSTALL_DIR" ] && ! is_directory_empty "$INSTALL_DIR"; then
    echo "The installation directory $INSTALL_DIR already exists and is not empty."
    read -p "Do you want to remove its contents and continue? (y/n): " choice
    case "$choice" in 
        y|Y ) 
            echo "Removing contents of $INSTALL_DIR"
            sudo rm -rf "$INSTALL_DIR"/.* 
            sudo rm -rf "$INSTALL_DIR"/*
            ;;
        n|N ) 
            echo "Installation cancelled. Please clear the directory manually and run the script again."
            exit 1
            ;;
        * ) 
            echo "Invalid input. Installation cancelled."
            exit 1
            ;;
    esac
fi

# Update and install dependencies
sudo apt update
sudo apt install -y git jq libtinfo5 wget net-tools

# Create temporary directory for downloading Leap
mkdir -p $TEMP_DIR
cd $TEMP_DIR

# Download and install Leap
wget https://github.com/AntelopeIO/leap/releases/download/v${LEAP_VERSION}/leap_${LEAP_VERSION}_amd64.deb
sudo dpkg -i leap_${LEAP_VERSION}_amd64.deb

# Clean up temporary directory
cd /
rm -rf $TEMP_DIR

# Prepare INSTALL_DIR
sudo mkdir -p $INSTALL_DIR
sudo chown $USER:$USER $INSTALL_DIR

# Clone the repository
git clone $REPO_URL $INSTALL_DIR

# Set up environment
echo "export PATH=$PATH:$INSTALL_DIR/libreNode" >> ~/.bashrc
echo "source $INSTALL_DIR/libreNode/node.env" >> ~/.bashrc
source $INSTALL_DIR/libreNode/node.env

# Create necessary directories
mkdir -p $DATADIR $SNAPDIR

# Update paths in start, stop, and snapstart scripts
sed -i "s|source /opt/libre-chain-nodes/libreNode/node.env|source $INSTALL_DIR/libreNode/node.env|g" $INSTALL_DIR/libreNode/start.sh
sed -i "s|source /opt/libre-chain-nodes/libreNode/node.env|source $INSTALL_DIR/libreNode/node.env|g" $INSTALL_DIR/libreNode/stop.sh
sed -i "s|source /opt/libre-chain-nodes/libreNode/node.env|source $INSTALL_DIR/libreNode/node.env|g" $INSTALL_DIR/libreNode/snapstart.sh

# Check and update ports in config.ini
CONFIG_FILE="$MAINDIR/config.ini"
HTTP_PORT=$(find_available_port 8888)
P2P_PORT=$(find_available_port 9876)

sed -i "s/http-server-address = 0.0.0.0:[0-9]*/http-server-address = 0.0.0.0:$HTTP_PORT/" $CONFIG_FILE
sed -i "s/p2p-listen-endpoint = 0.0.0.0:[0-9]*/p2p-listen-endpoint = 0.0.0.0:$P2P_PORT/" $CONFIG_FILE

echo "HTTP port set to: $HTTP_PORT"
echo "P2P port set to: $P2P_PORT"

# Update config.ini with peers
cat $MAINDIR/peers.ini >> $CONFIG_FILE

# Start the node with snapshot
cd $MAINDIR
./snapstart.sh

echo "Installation complete. Please run 'source ~/.bashrc' to update your environment."
echo "Your node is now starting. Check logs with 'tail -f $DATADIR/stderr.txt'"
echo "HTTP endpoint: http://localhost:$HTTP_PORT"
echo "P2P endpoint: localhost:$P2P_PORT"